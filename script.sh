#!/bin/bash
#Code to change resolution of Raspberry Pi's. Useful for those with 480p screens.

#Ask and change code
function askChange
{
        echo "Change resolution?"
        echo "(0) 480p"
        echo "(1) 720p"
        echo "(2) 1080p"
        echo "(3) No"
        read -n1 ans
        if [ "$ans" == "0" ]
        then
                sed -i '$ d' config.txt
                echo "hdmi_cvt 800 480 60 6 0 0 0" >> config.txt
                echo "Done"
        elif [ "$ans" == "1" ]
        then
                sed -i '$ d' config.txt
                echo "hdmi_cvt 1280 720 60 6 0 0 0" >> config.txt
                echo "Done"
        elif [ "$ans" == "2" ]
        then
                sed -i '$ d' config.txt
                echo "hdmi_cvt 1920 1080 60 6 0 0 0" >> config.txt
                echo "Done"
        elif [ "$ans" == "3" ]
        then
                echo "Alright, cya later :D"
        else
                echo "Not a real thing I asked for, please run again"
        fi
}

last_line=$(tail --lines 1 config.txt)

if [ "$last_line" == "hdmi_cvt 800 480 60 6 0 0 0" ]
then
        echo "480p"
        askChange
elif [ "$last_line" == "hdmi_cvt 1280 720 60 6 0 0 0" ]
then
        echo "720p"
        askChange
elif [ "$last_line" == "hdmi_cvt 1920 1080 60 6 0 0 0" ]
then
        echo "1080p"
        askChange
else
        echo "Something went wrong, you'll need to edit this yourself to be safe"
fi
